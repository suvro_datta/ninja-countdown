=== Ninja Countdown Builder | Fastest Countdown Builder ===
Contributors: lovelightplugins
Tags: Countdown, Countdown Builder, Countdown Timer, Timer
Requires at least: 3.4
Tested up to: 5.7.1
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html 

Display clean, customizable, and responsive Countdown To Your Wordpress Site.

== Description ==

The best way of customizable Countdown timer. You can set the Countdown timer the way you imagine. 

= Why Ninja Countdown ? =

* Fastest Ninja Countdown Builder is the Fastest Countdown builder.
* Simplest **simple to set up**
* Lightweight No un-used assets loads. Assets size is just 10kb.
* Organized the plugin is well Organized to set up a new timer.
* Live Preview You can edit and show live preview 

== Frequently Asked Questions ==

= Is The Countdown customizable? =
Yes, You can customize everything of the Countdown timer.

== Screenshots ==

1. Completely customizable with live preview editor.
2. Show the Countdown in specific Page
3. Your customized Countdown in frontend

== Changelog ==

= 1.0.0 =
* Launched the Ninja Countdown Timer!